**Desarrollo de aplicaciones web**

5to semestre 5AVP

López Sánchez Ary Daniel

- Practica #1 - 02/09/2022 - Práctica de ejemplo
Commit: a6b55f290c3b202ec1cbd0238e38f8d3a92d0a62
Archivo: https://gitlab.com/AryDaniel/desarrolla_apicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

- Practica 2 09/09/2022 Mensaje en JavaScript
Commit: 54ff01ab5df92169f5f9f0615edf04bd41174f45
Archivo: https://gitlab.com/AryDaniel/desarrolla_apicaciones_web/-/blob/main/parcial1/practicaJavaScript.html

- Practica #3 - 05/09/2022 - Practica Web y datos
Commit: 9c0a737618734d8e62a15825778b4e06049ec4b2
Archivo: https://gitlab.com/AryDaniel/desarrolla_apicaciones_web/-/blob/main/parcial1/PracticaWebDatos/PracticaWebDatos.rar

- Practica #4 - 19/09/2022 - consultar Datos con PHP
Commit: 46cc1ac95be25ef4bdb2cd5b3bfd322395cb1090
Archivo: https://gitlab.com/AryDaniel/desarrolla_apicaciones_web/-/blob/main/parcial1/PracticaWebDatos/consultarDatos.php

- Practica #5 - 22/09/2022 - Vista de registrar datos
Commit: 994f0cbcf67e9a6b09a52e408238b5bea4cde747
Archivo: https://gitlab.com/AryDaniel/desarrolla_apicaciones_web/-/blob/main/parcial1/PracticaWebDatos/RegistrarDatos.html

- Practica #6 - 26/09/2022 - Conexion
Commit: f1838b075722e0bb5e7323ebe72a8fc4ee4f18b6
Archivo: https://gitlab.com/AryDaniel/desarrolla_apicaciones_web/-/blob/main/parcial1/PracticaWebDatos/conexion.php
